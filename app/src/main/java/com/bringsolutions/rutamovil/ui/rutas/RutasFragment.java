package com.bringsolutions.rutamovil.ui.rutas;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.bringsolutions.rutamovil.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class RutasFragment extends Fragment {
	
	View view;
	private GoogleMap mMap;
	private SupportMapFragment mapFragment;
	private Spinner spTransportes, spRutas;
	
	
	
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_rutas, container, false);
		inicializarElementos();
		clicks();
		
		mapFragment.getMapAsync(new OnMapReadyCallback() {
			@Override
			public void onMapReady(GoogleMap googleMap) {
				mMap = googleMap;
				
				/* Add a marker in Sydney and move the camera
				LatLng sydney = new LatLng(-34, 151);
				mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
				mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/
			}
	});
		
		return view;
	}
	
	private void clicks() {
		
		spTransportes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				
				switch (position){
					case 0://sintransporte
						ArrayAdapter adaptadorSinTrans = ArrayAdapter.createFromResource( getActivity(), R.array.no_trasnporte_elegido , android.R.layout.simple_dropdown_item_1line);
						spRutas.setAdapter(adaptadorSinTrans);
						break;
					case 1://cocacola
						ArrayAdapter adaptadorCocacola = ArrayAdapter.createFromResource( getActivity(), R.array.array_rutas_cocacola , android.R.layout.simple_spinner_dropdown_item);
						spRutas.setAdapter(adaptadorCocacola);
						break;
					case 2://bimbo
						ArrayAdapter adaptadorBimbo = ArrayAdapter.createFromResource( getActivity(), R.array.array_rutas_bimbo , android.R.layout.simple_spinner_dropdown_item);
						spRutas.setAdapter(adaptadorBimbo);
						break;
					case 3://gamesa
						ArrayAdapter adaptadorGamesa = ArrayAdapter.createFromResource( getActivity(), R.array.array_rutas_gamesa , android.R.layout.simple_spinner_dropdown_item);
						spRutas.setAdapter(adaptadorGamesa);
						
						break;
				}
				
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			
			}
		});
		
		spRutas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				
				switch (position){
					case 1://cocacola
						LatLng pA = new LatLng(17.987315, -92.988754);
						LatLng pB = new LatLng(17.998699, -92.916846);
						mMap.addMarker(new MarkerOptions().position(pA).title("Inicio Ruta 1"));
						mMap.addMarker(new MarkerOptions().position(pB).title("Fin Ruta 1"));
						mMap.moveCamera(CameraUpdateFactory.newLatLng(pA));
						
						
						break;
					
				}
				
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			
			}
		});
		
		
		
		
	}
	
	private void inicializarElementos() {
		//SupportMapFragment mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.mapila);
		mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapila);
		spTransportes = view.findViewById(R.id.spTransportes);
		spRutas = view.findViewById(R.id.spRutas);
		
	}
	
}